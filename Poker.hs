module Poker where

quatroIguais :: [Int] -> Int
quatroIguais [] = 0
quatroIguais (x:xs) = if (xs /= []) then
                          if (x == head xs) then
                              1 + quatroIguais xs
                          else
                              quatroIguais xs
                      else
                          0


todosDiferentes :: [Int] -> Int
todosDiferentes [] = 0
todosDiferentes (x:xs) | ((sequencia (x:xs) /= 4) && (quatroIguais (x:xs) == 0)) = 1
                       | otherwise = 0


sequencia :: [Int] -> Int
sequencia [] = 0
sequencia (x:xs) = if (xs /= []) then
                       if (x + 1 == head xs) then
                           1 + sequencia xs
                       else
                           sequencia xs
                   else
                       0


calcular :: [[Int]] -> (Int, Int, Int, Int) -> (Int, Int, Int, Int)
calcular [] t = t
calcular (x:xs) (qI, tD, s, nd) | (quatroIguais x == 3) =  calcular xs ((qI + 1), tD, s, nd)
                                | (todosDiferentes x == 1) = calcular xs (qI, (tD + 1), s, nd)
                                | (sequencia x == 4) = calcular xs (qI, tD, (s + 1), nd)
                                | otherwise = calcular xs (qI, tD, s, (nd + 1))