module Arquivos (valorarMao, limparMao, getHand, fromIOToString, ler, escrever, ordenarMao) where

import System.IO
import Data.Char
import Data.List.Split
import Data.List
import System.IO.Unsafe

{-
ler recebe uma tupla com o caminho do arquivo a ser lido e um manipulador do arquivo e devolve uma String que é a linha lida na nova posição do manipulador.

Obtem-se o tamanho do arquivo e a posição atual do manipulador, se a posição atual for o fim do arquivo, a execução é encerrada, caso contrário, o manipulador é movido para a posição de leitura da próxima linha


ler :: (FilePath, IO Handle)-> String
ler (path, E0) = do
    file <- openFile path ReadMode
    
    tam <- hFileSize file
    pos <- hTell file

    if (pos == tam) then
        return ()
    
    else do
        hSeek file AbsoluteSeek pos

        conteudo <- hGetLine file
        
        --tratar(remEsp conteudo)
        
        ler (path, file)

    hClose file

ler (path, file) = do
    tam <- hFileSize file
    pos <- hTell file

    if (pos == tam) then
        return ()
    
    else do
        hSeek file AbsoluteSeek pos

        conteudo <- hGetLine file
        
        --tratar(remEsp conteudo)
        
        ler (path, file)

    hClose file


-----------------------------------------------------
getFileContents fileHandle = do                     |
    pos <- hTell fileHandle                         |
                                                    |
    hSeek fileHandle AbsoluteSeek pos               |
                                                    |
    EOF <- hIsEOF fileHandle                        |
                                                    |
    if EOF then                                     |
        return ()                                   |
                                                    |
    else                                            |
        do                                          |
          info <- hGetLine fileHandle               |
          putStrLn info                             |
          getFileContents fileHandle                |
-----------------------------------------------------
main = do                                           |
        fileHandle <- openFile "teste.txt" ReadMode |
        getFileContents fileHandle                  |
        hClose fileHandle                           |
-----------------------------------------------------
ler = do                                            |
    file <- readFile "teste.txt"                    |
    mapM_ putStrLn remEsp(lines file)               |
-----------------------------------------------------
-}

ler :: String -> IO String
ler [] = error "Informe um arquivo."
ler path = do
    conteudo <- readFile path
    return $ remEsp conteudo


--fromIOToString :: IO [String] -> String
fromIOToString all = unsafePerformIO all


getHand :: String -> [String]
getHand buffer = splitOn "\n" buffer


limparMao :: [[Char]] -> [[Char]]
limparMao [] = []
limparMao (x:xs) | (x == "") = limparMao xs
                 | otherwise = x : limparMao xs


remEsp :: String -> [Char]
remEsp [] = []
remEsp (x:xs) | (x == ' ') = remEsp xs
              | otherwise = x : remEsp xs


ordenarMao :: [[Int]] -> [[Int]]
ordenarMao [] = []
ordenarMao (x:xs) = sort x : ordenarMao xs


valorarMao :: [String] -> [[Int]]
valorarMao [] = []
valorarMao (x:xs) = aux_VM x : valorarMao xs

aux_VM :: [Char] -> [Int]
aux_VM [] = []
aux_VM (x:xs) = if (isDigit x) then
                    (digitToInt x) : aux_VM xs
                else
                    subs x : aux_VM xs
                        where subs c | (ord c == 84) = 10
                                     | (ord c == 74) = 11
                                     | (ord c == 81) = 12
                                     | (ord c == 75) = 13
                                     | (ord c == 65) = 14
                                     | otherwise = ord c


escrever :: String -> (Int, Int, Int, Int) -> Int -> IO ()
escrever tempo (qI, tD, s, nd) n | (n == 3) = writeFile "saida.txt" (tempo ++ " " ++ show(qI) ++ " " ++ show(tD) ++ " " ++ show(s) {- Descomentar para verificar a corretude ++ " " ++ show(nd))-} ++ "\n")
                                 | otherwise = appendFile "saida.txt" (tempo ++ " " ++ show(qI) ++ " " ++ show(tD) ++ " " ++ show(s) {- Descomentar para verificar a corretude ++ " " ++ show(nd))-} ++ "\n")