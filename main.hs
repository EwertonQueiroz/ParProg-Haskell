module Main where

import Arquivos
import Poker
import Data.Time.Clock.POSIX
import System.Environment
import System.Exit

main :: IO ()
main = do
    argv <- getArgs
    
    let start = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print start
    escrever (show(fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime)) - start)) (calcular(ordenarMao(valorarMao(limparMao(getHand(fromIOToString(ler (head argv))))))) (0, 0, 0, 0)) 3
    let end = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print end
    
    let start = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print start
    escrever (show(fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime)) - start)) (calcular(ordenarMao(valorarMao(limparMao(getHand(fromIOToString(ler (head(tail argv)))))))) (0, 0, 0, 0)) 2
    let end = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print end
    
    let start = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print start
    escrever (show(fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime)) - start)) (calcular(ordenarMao(valorarMao(limparMao(getHand(fromIOToString(ler (head(tail(tail argv))))))))) (0, 0, 0, 0)) 1
    let end = fromIntegral(fromIOToString(round `fmap` Data.Time.Clock.POSIX.getPOSIXTime))
    print end